import static org.hamcrest.CoreMatchers.is;

import org.junit.Before;
import org.junit.Test;

import models.Destinos.Destino;
import models.Destinos.DestinoConcreto;
import play.test.Fixtures;
import play.test.UnitTest;

public class DestinosTest extends UnitTest {

	@Before
	public void setup() {
		Fixtures.deleteDatabase();
	}
	

	@Test
	public void creaDestino() {
		
		Destino admision = new DestinoConcreto("Admisión","192.168.1.84",6100);

		assertThat(admision.nombre, is("Admisión"));
		assertThat(admision.direccion, is("192.168.1.84"));
		assertThat(admision.port, is(6100));
	}
	
	@Test
	public void cambiarDestino() {
		
		Destino admision = new DestinoConcreto("Admisión","192.168.1.84",6100);

		assertThat(admision.nombre, is("Admisión"));
		assertThat(admision.direccion, is("192.168.1.84"));
		assertThat(admision.port, is(6100));
	}
	
	
	
}
