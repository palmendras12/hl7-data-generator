import static org.junit.Assert.assertThat;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import models.Rut;
import models.Eventos.MensajeADT01;
import tcpserver.MensajeHL7;

public class UnitTestADT {

	String mensaje = "MSH|^~\\&|AGENDA|MC|HIS|MC|201808122322||ADT^A01|HL7MSG00001|T|2.3|" + "EVN|A01|201808122322||"
			+ "PID|||MRN12345^168636490^M11||COLOMA^CRISTIAN|PADILLA|19880724|M||M|AVENIDA^SANTA ROSA^326^SANTIAGO|56||(2)226120083||S||MRN12345001^82439846^M10|SS12345|15260127^STGO|"
			+ "NK1|1|PALMA^LESLIE^VALENZUELA|ESPOSA|||||||1|I|19910121||||AVENIDA^SANTA ROSA^326^SANTIAGO|||CHILENA||||ADM|A0|";

	/*
	 * @Test (expected = IllegalStateException.class) public void creaRutInvalido()
	 * {
	 * 
	 * }
	 */

	@Test
	public void parseaADT01() {

		MensajeHL7 msg = MensajeHL7.of(mensaje);
		MensajeADT01 m = (MensajeADT01) msg;
		System.out.println(mensaje);
		//MSH
		assertThat(m.aplicaciondeEnvio, is("AGENDA"));
		assertThat(m.facilitadorEnvio, is("MC"));
		assertThat(m.aplicacionRecepcion, is("HIS"));
		assertThat(m.facilitadorEnvio, is("MC"));
		assertThat(m.fechaMensaje, is("201808122322"));
		assertThat(m.tipoMensaje, is("ADT^A01"));

		//EVN
		assertThat(m.fechaEvento, is("201808122322"));
		assertThat(m.tipoEvento, is("A01"));

		//PID
		assertThat(m.idPacienteNacional, is("MRN12345"));
		assertThat(m.idPacienteDNI, is("168636490"));
		assertThat(m.idPacienteInterno, is("M11"));
		
		assertThat(m.primerNombrePaciente, is("CRISTIAN"));
		assertThat(m.apellidoNombrePaciente, is("COLOMA"));
		assertThat(m.segundoApellidoNombrePaciente, is("PADILLA"));
		assertThat(m.fechaNacimientoPaciente, is("19880724"));
		assertThat(m.sexoPaciente, is("M"));
		assertThat(m.razaPaciente, is("M"));
		
		assertThat(m.direccionTipoVia, is("AVENIDA"));
		assertThat(m.direccionNombreVia, is("SANTA ROSA"));
		assertThat(m.direccionNumero, is("326"));
		assertThat(m.direccionCiudad, is("SANTIAGO"));
		assertThat(m.codigoPais, is("56"));
		assertThat(m.telefonoPaciente, is("(2)226120083"));
		assertThat(m.estadoMarital, is("S"));

		//NK1
		



	}

}
