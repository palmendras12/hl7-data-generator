import static org.junit.Assert.assertThat;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;

import org.junit.Test;


import models.Rut;

public class UnitTestRut {

	@Test
	public void creaRut() {
		
		Rut rut = new Rut("17990512-4");

		assertThat(rut.rut, is("17990512"));
		assertThat(rut.numVerificador, is("4"));

		Rut rut2 = new Rut("17.990.512-4");

		assertThat(rut2.rut, is("17990512"));
		assertThat(rut2.numVerificador, is("4"));
		
		Rut rut3 = Rut.of("17990512-4");
		Rut rut4 = Rut.of("17.990.512-4");
		assertThat(rut3.rut, is("17990512"));
		assertThat(rut3.numVerificador, is("4"));
		assertThat(rut4.rut, is("17990512"));
		assertThat(rut4.numVerificador, is("4"));

	}
	
	@Test (expected = IllegalStateException.class)
	public void creaRutInvalido() {
		
		Rut rut = new Rut("17990512-5");

	}
	@Test (expected = IllegalStateException.class)
	public void creaRutOfInvalido() {
		
		Rut rut = Rut.of("17.990.512-5");

	}
	

}
