Java application that will receive HL7 messages and will simulate a EHR.

The app was developed to show how HL7 messagges trigger actions in a system.
Assitants will send predeterminated HL7 messages to the system and simulate a typical medical visit.

Basics of system:
ADT A04 will register a new patient in the system.
ADT A01 will add a admit event to the patient history
ADT A03 will add a discharge event to the patient history
ADT A08 will update patient information


ORU-R01 will add a observations and result to patient.

Assitants will recieve ack messages in their terminals.

"Mirth Connect Workshop"  for the CAIB 2018 www.caib.cl