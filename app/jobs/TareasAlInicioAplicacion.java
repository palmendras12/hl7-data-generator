package jobs;

import models.Usuario;
import models.Destinos.Examen;
import models.Destinos.TipoDestino;
import models.Destinos.TipoDestinoAdmision;
import models.Destinos.TipoDestinoAtencionMedico;
import models.Destinos.TipoDestinoFichaMaestra;
import models.Destinos.TipoDestinoLaboratorio;
import play.Logger;
import play.Play;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import tcpserver.HL7Server;
import tcpserver.MensajeHL7;
import utils.NetworkUtils;

@OnApplicationStart
public class TareasAlInicioAplicacion extends Job {

	@Override
	public void doJob() throws Exception {

		/*
		 * long nPaises = Pais.count(); if (nPaises == 0) { //
		 * Logger.debug("Cargando paises"); Fixtures.deleteDatabase();
		 * Fixtures.loadModels("paises.yml"); }
		 */

		if (Play.runingInTestMode()) {
			/*
			 * TipoDispositivo tipo = new TipoDispositivoPulseraH10(); tipo.save();
			 * Dispositivo disp = tipo.crearDispositivoMacRandom(); disp.imei =
			 * Long.MAX_VALUE; disp.save(); JPA.em().getTransaction().commit();
			 * JPA.em().getTransaction().begin();
			 */

		}

		if (Play.runingInTestMode()) {
			return;

		}

		if (Usuario.count() == 0) {
			cargarUsuariosPrueba();

		}

		if (TipoDestino.count() == 0) {
			crearTiposDestino();
		}
		try {
			if (NetworkUtils.isPortAvailable(MensajeHL7.PORT)) {
				HL7Server hl7Sender = new HL7Server();
				hl7Sender.now();
			}
		} catch (Exception e) {
			Logger.error(e.getMessage());
		}

	}

	private void crearTiposDestino() {
		crearTipoAdmision();
		crearTipoAtencionMedico();
		crearTipoLaboratorio();
		crearFichaMaestra();
	}

	private void cargarUsuariosPrueba() {

		Usuario user = new Usuario("pablo@centinela.cl", "123456", "Pablo Almendras");
		user.isAdmin = true;
		user.save();
	}

	private void crearTipoAdmision() {
		TipoDestino tipoAdmision = new TipoDestinoAdmision();
		tipoAdmision.nombre = "Admision";
		tipoAdmision.save();
	}

	private void crearTipoAtencionMedico() {
		TipoDestino tipoAtencion = new TipoDestinoAtencionMedico();
		tipoAtencion.nombre = "Atencion";
		tipoAtencion.save();
	}

	private void crearTipoLaboratorio() {
		TipoDestino tipoLaboratorio = new TipoDestinoLaboratorio();
		tipoLaboratorio.nombre = "Laboratorio";
		tipoLaboratorio.save();
	}


	private void crearFichaMaestra() {
		TipoDestino tipoFichaMaestra = new TipoDestinoFichaMaestra();
		tipoFichaMaestra.nombre = "Ficha Maestra";
		tipoFichaMaestra.save();
	}
}
