package tcpserver;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.Scanner;
public class MyClientSocket {
    private Socket socket;
    public MyClientSocket(InetAddress serverAddress, int serverPort) throws Exception {
        this.socket = new Socket();
        this.socket.connect(new InetSocketAddress(serverAddress, serverPort), 500);
    }
    public void start() throws IOException {
        while (true) {
            PrintWriter out = new PrintWriter(this.socket.getOutputStream(), true);
            out.println("Hola Mundo Socket");
            out.flush();
            
           
            this.socket.shutdownInput();
            this.socket.shutdownOutput();
            this.socket.close();
        }
        
    }
    
    public void sendMsgTCP(String mensaje) throws IOException {
        while (true) {
            PrintWriter out = new PrintWriter(this.socket.getOutputStream(), true);
            out.println(mensaje);
            out.flush();
            
           
            this.socket.shutdownInput();
            this.socket.shutdownOutput();
            this.socket.close();
        }
        
    }
    
    public Boolean isConnected() {
    	return this.socket.isConnected();   	 		 	 
    }
    
    public void closeSocket() throws IOException {
    	this.socket.close();
	}
    
}