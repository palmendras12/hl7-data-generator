package tcpserver;

import static tcpserver.ConstantesHL7.ADT01;
import static tcpserver.ConstantesHL7.ORM01;
import static tcpserver.ConstantesHL7.ORU01;

import java.nio.charset.Charset;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;

import models.Paciente;
import models.Eventos.Evento;
import models.Eventos.MensajeADT01;
import play.Logger;
import play.db.jpa.JPA;

public class HL7ProtocolHandler extends GenericChannelHandler {

	public HL7ProtocolHandler() {
		super();
		Logger.trace("En constructor de %s", this.getClass().getSimpleName());
	}

	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {

		Channel ch = e.getChannel();
		ChannelBuffer buff = (ChannelBuffer) e.getMessage();
		String line = buff.toString(Charset.defaultCharset());

		Logger.debug("Linea desde %s recibida por TCP: %s", ctx.getChannel().getRemoteAddress().toString(),line);
		procesarMensaje(line, ch, ctx.getChannel().getRemoteAddress().toString());
		JPA.em().flush();
		JPA.em().getTransaction().commit();
		JPA.em().getTransaction().begin();

	}

	private void procesarMensaje(String line, Channel ch, String ipOrigen) {

		MensajeHL7 m = MensajeHL7.of(line);
		m.ipOrigen = ipOrigen;

		switch (m.comando) {

		case ADT01: {

			Logger.debug("llego un ADT^A01");
			m.realizarAccion();
			if (m.valido) {
				MensajeADT01 adt01 = (MensajeADT01) m;
				//Si no existe creamos paciente.
				Paciente paciente = Paciente.findByIdAlternativo(adt01.idAlternativo);
				if (paciente == null) {
					paciente = new Paciente(adt01.primerNombrePaciente +" " + adt01.apellidoNombrePaciente, adt01.idPacienteDNI, adt01.idAlternativo);
					Logger.debug("Paciente no existía. Se ha creado en el sistema con ID: %s", paciente.idAlternativo);
					paciente.save();
					Evento evento = new Evento(String.format("Nuevo paciente creado ID: %s", paciente.idAlternativo),ipOrigen);
					evento.save();
				}
				
				else {
					Logger.debug("Paciente %s. ya existe en el sistema", paciente.idAlternativo);
					Evento evento = new Evento(String.format("Intentando crear un paciente que ya existe en el sistema: ID: %s", paciente.idAlternativo),ipOrigen);
					evento.save();
				}
			}
			//String msg = m.respuesta();
			//enviaMensaje(ch, msg);
			break;
		}
		case ORM01: {

			Logger.debug("llego un ORM^O01");
			m.realizarAccion();
			//String msg = m.respuesta();
			//enviaMensaje(ch, msg);
			break;
		}
		
		case ORU01: {

			Logger.debug("llego un %s",m.comando);
			m.realizarAccion();
			//String msg = m.respuesta();
			//enviaMensaje(ch, msg);
			break;
		}


		default: {
			//String msg = m.respuesta();
			//m.imei = this.imei;
			//enviaMensaje(ch, msg);
			break;
		}

		}


		ultimoComando = m.comando;
				
		//m.ejecutarFuncionSIcorresponde();
		// l.save();
		Logger.debug("Comando: %s Valido: %s", m.comando, m.valido);
		if (m.comando!=null && m.valido)
			m.save();
		else
			Logger.debug("Mensaje fue catalogado como no valido. No se guarda");

	}

}
