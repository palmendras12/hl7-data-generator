package tcpserver;

public class ConstantesHL7 {

	public static final String endOfCommand = "#";

	public static final String ADT01 = "ADT^A01";
	public static final String IWAP01 = "IWAP01";
	public static final String ORM01 = "ORM^O01";
	public static final String ORU01 = "ORU^R01";
	
}
