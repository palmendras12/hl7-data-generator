package tcpserver;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;

import org.jboss.netty.channel.Channel;

import models.Eventos.MensajeADT01;
import models.Eventos.MensajeORM01;
import models.Eventos.MensajeORU01;
import play.db.jpa.Model;
import static tcpserver.ConstantesHL7.ADT01;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Entity
@Inheritance
public abstract class MensajeHL7 extends Model{
	static public int PORT = 8070;
	
	public String comando;
	public Boolean valido=false;

	public String idMensaje;

	public String ipOrigen;
	public MensajeHL7(String linea) {
		
		String[] split = linea.split("\\|");
		if (split.length < 2)
			return;
		this.comando = split[8];
		this.idMensaje = split[9];


	}
	
	public static MensajeHL7 of(String line) {
		String[] split = line.split("\\|");
		if (split.length < 2)
			return null;
		String cmd = split[8];
		if (cmd.equals(ConstantesHL7.ADT01))
			return new MensajeADT01(line);
		if (cmd.equals(ConstantesHL7.ORM01))
			return new MensajeORM01(line);
		if (cmd.equals(ConstantesHL7.ORU01))
			return new MensajeORU01(line);
		else
			return null;
	}
	
	public abstract void realizarAccion();
}
