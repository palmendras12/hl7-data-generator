package tcpserver;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import play.Logger;
import play.jobs.Job;

public class HL7Server extends Job {


	@Override
	public void doJob() {

		ChannelFactory factory = new NioServerSocketChannelFactory(Executors.newCachedThreadPool(),
				Executors.newCachedThreadPool());

		ServerBootstrap bootstrap = new ServerBootstrap(factory);

		// Logger.debug("JPA abierta: %s", JPA.em().isOpen());

		bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
			public ChannelPipeline getPipeline() {
//				JPAPlugin.startTx(false);
				Logger.debug("Creando nuevo handler de pulsera");
				HL7ProtocolHandler hl7ProtocolHandler = new HL7ProtocolHandler();
				//SocketMonitorService.getInstance().addSocket(pulseraH10ProtocolHandler);
				return Channels.pipeline(hl7ProtocolHandler);
			}
		});

		bootstrap.setOption("child.tcpNoDelay", true);
		bootstrap.setOption("child.keepAlive", true);

		// Logger.debug("doJob en " + this.getClass().getName());

		bootstrap.bind(new InetSocketAddress(ConfigHL7.PORT));
	}

}
