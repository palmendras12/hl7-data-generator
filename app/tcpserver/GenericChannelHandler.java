package tcpserver;

import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.SimpleChannelHandler;

import play.Logger;
import play.db.jpa.JPAPlugin;

public class GenericChannelHandler extends SimpleChannelHandler {

	public DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	public String fecha = sdf.format(new Date());

	public String ultimoComando = null;
	public Long imei;
	public Integer modoTrabajo;
	ChannelHandlerContext channelHandlerContext;

	@SuppressWarnings("deprecation")
	@Override
	public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
		super.channelConnected(ctx, e);
		this.channelHandlerContext = ctx;
		Logger.trace("channelConnected %s", this.getClass().getSimpleName());
		JPAPlugin.startTx(false);
	}

	@Override
	public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
		super.channelOpen(ctx, e);
		Logger.trace("channelOpen %s", this.getClass().getSimpleName());
	}

	@Override
	public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {

		super.channelDisconnected(ctx, e);
		Logger.trace("channelDisconnected %s", this.getClass().getSimpleName());
	}

	@Override
	public void closeRequested(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
		super.closeRequested(ctx, e);
		Logger.trace("closeRequested %s", this.getClass().getSimpleName());
	}

	@SuppressWarnings("deprecation")
	@Override
	public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
		super.channelClosed(ctx, e);
		JPAPlugin.closeTx(false);
		Logger.trace("Prueba H10");
		Logger.trace("+++channelClosed %s", this.getClass().getSimpleName());
		//SocketMonitorService.getInstance().removeSocket(this);
	}

	@Override
	public String toString() {
		return "GenericChannelHandler [sdf=" + sdf + ", fecha=" + fecha + ", ultimoComando=" + ultimoComando + ", imei="
				+ imei + ", modoTrabajo=" + modoTrabajo + "]";
	}

	public void enviarBP33(String modoTrabajo) {

		Channel ch = channelHandlerContext.getChannel();
		String msg = String.format("IWBP33,%d,080835,%d#");
		Logger.trace("Enviando msg");
		enviaMensaje(ch, msg);

	}

	public void enviarBP12(String numeros) {

		Channel ch = channelHandlerContext.getChannel();
		enviaMensaje(ch, String.format("IWBP12,%d,080835,%s#", imei, numeros));

	}
	
	public void enviarBP14(String listaBlanca) {
		
		Channel ch = channelHandlerContext.getChannel();
		enviaMensaje(ch, String.format("IWBP14,%d,080835%s#", imei,listaBlanca));
	}

	String buildMessageWithNewline(String... args) {

		String s = Arrays.asList(args).stream().collect(Collectors.joining(","));
		return "IW" + s + "#\n";
	}

	protected void enviaMensaje(Channel ch, String s) {
		if (!s.endsWith("\n"))
			s += "\n";

		ChannelBuffer chb = ChannelBuffers.copiedBuffer(s, Charset.defaultCharset());
		ch.write(chb);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
		e.getCause().printStackTrace();

		Channel ch = e.getChannel();
		ch.close();
	}

}
