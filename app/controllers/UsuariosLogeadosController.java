package controllers;

import models.Usuario;
import play.Play;
import play.mvc.Before;

public class UsuariosLogeadosController extends GlobalController {

	@Before(priority = 1)
	static void validaUsuarioConectado() {

		renderArgs.put("reactMode", Play.configuration.getProperty("reactMode", "dev"));

		if (controllers.Secure.Security.isConnected()) {
			Usuario persona = Usuario.find("byEmail", controllers.Secure.Security.connected()).first();
			renderArgs.put("user", persona);
		} else
			redirect("/login");
	}

	protected static Usuario currentUser() {
		Usuario persona = Usuario.find("byEmail", controllers.Secure.Security.connected()).first();
		return persona;
	}

	public static void home() {

		if (currentUser().esCentinela())
			PanelController.index();
		else
			Admin.index();


	}
}
