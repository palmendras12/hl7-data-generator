package controllers;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;

import models.Paciente;
import models.Destinos.Destino;
import models.Destinos.DestinoLaboratorio;
import models.Destinos.Examen;
import models.Eventos.Evento;
import models.Eventos.MensajeADT01;
import models.Eventos.MensajeORM01;
import play.Logger;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.db.jpa.JPABase;
import play.mvc.Before;
import play.mvc.Controller;

public class ApplicationSmart extends Controller {

	@Before
	static void addDefaults() {

	}

	public static void index() {

		render();
	}

	public static void seleccionaSexo() {
		
		render();
	}

	public static void muestraListaSexo(String sexo) {
		if (sexo.equals("male"))
			render(sexo);
		else if(sexo.equals("female"))
			render(sexo);
	
	}
	
	public static void seleccionaRut() {
		
		render();
	}

	public static void buscarPorRut(String rut) {
	
		render(rut);
	}

	public static void creaPaciente() {
	
		render();
	}

	public static void crearJsonPaciente(String nombre, String apellido, String rut) {
	
		String patientJson="{\n" + 
				"  \"resourceType\": \"Patient\",\n" + 
				"  \"text\": {\n" + 
				"    \"status\": \"generated\",\n" + 
				"    \"div\": \"<div xmlns='http://www.w3.org/1999/xhtml'>Ejercicio 2<a name='mm'/></div>\"\n" + 
				"  },\n" + 
				"  \"id\": \"cf-1540407160672\",\n" + 
				"  \"_birthPlace\": {\n" + 
				"    \"extension\": [],\n" + 
				"    \"url\": \"http://hl7.org/fhir/StructureDefinition/birthPlace\",\n" + 
				"    \"valueAddress\": \"Brasil\"\n" + 
				"  },\n" + 
				"  \"name\": [\n" + 
				"    {\n" + 
				"      \"use\": \"official\",\n" + 
				"      \"prefix\": [\n" + 
				"        \"Sra\"\n" + 
				"      ],\n" + 
				"      \"given\": [\n" + 
				"        "+nombre+"\",\n" + 
				"        \"\"\n" + 
				"      ],\n" + 
				"      \"_family\": {\n" + 
				"        \"extension\": [\n" + 
				"          {\n" + 
				"            \"url\": \"https://www.hl7.org/fhir/extension-humanname-mothers-family.html\",\n" + 
				"            \"valueString\": \""+apellido+"\"\n" + 
				"          }\n" + 
				"        ]\n" + 
				"      },\n" + 
				"      \"family\": \"Silva\",\n" + 
				"      \"text\": \"Larisa Oliva Silva Sousa\"\n" + 
				"    }\n" + 
				"  ],\n" + 
				"  \"gender\": \"female\",\n" + 
				"  \"birthDate\": \"1975-07-29\",\n" + 
				"  \"maritalStatus\": {\n" + 
				"    \"coding\": [\n" + 
				"      {\n" + 
				"        \"code\": \"M\",\n" + 
				"        \"system\": \"http://hl7.org/fhir/v3/MaritalStatus\",\n" + 
				"        \"display\": \"Married\"\n" + 
				"      }\n" + 
				"    ]\n" + 
				"  },\n" + 
				"  \"address\": [\n" + 
				"    {\n" + 
				"      \"use\": \"home\",\n" + 
				"      \"type\": \"physical\",\n" + 
				"      \"line\": [\n" + 
				"        \"Avenida José Miguel Carrera\",\n" + 
				"        \"1325\"\n" + 
				"      ],\n" + 
				"      \"city\": \"Santiago\",\n" + 
				"      \"district\": \"San Miguel\",\n" + 
				"      \"country\": \"Chile\",\n" + 
				"      \"text\": \"Avenida José Miguel Carrera 1325, San Miguel, Santiago\"\n" + 
				"    }\n" + 
				"  ],\n" + 
				"  \"identifier\": [\n" + 
				"    {\n" + 
				"      \"value\": \""+rut+"\",\n" + 
				"      \"system\": \"https://registrocivil.cl/RUT\"\n" + 
				"    }\n" + 
				"  ],\n" + 
				"  \"communication\": [\n" + 
				"    {\n" + 
				"      \"preferred\": true,\n" + 
				"      \"language\": {\n" + 
				"        \"coding\": [\n" + 
				"          {\n" + 
				"            \"code\": \"pt-BR\",\n" + 
				"            \"system\": \"urn:ietf:bcp:47\",\n" + 
				"            \"display\": \"Portugués\"\n" + 
				"          }\n" + 
				"        ],\n" + 
				"        \"text\": \"Portugués (Brasil)\"\n" + 
				"      }\n" + 
				"    }\n" + 
				"  ],\n" + 
				"  \"contact\": [\n" + 
				"    {\n" + 
				"      \"name\": {\n" + 
				"        \"use\": \"official\",\n" + 
				"        \"prefix\": [\n" + 
				"          \"Sr\"\n" + 
				"        ],\n" + 
				"        \"given\": [\n" + 
				"          \"Joan\",\n" + 
				"          \"Paulo\"\n" + 
				"        ],\n" + 
				"        \"family\": \"Alves\",\n" + 
				"        \"_family\": {\n" + 
				"          \"extension\": [\n" + 
				"            {\n" + 
				"              \"url\": \"https://www.hl7.org/fhir/extension-humanname-mothers-family.html\",\n" + 
				"              \"valueString\": \"Salas\"\n" + 
				"            }\n" + 
				"          ]\n" + 
				"        },\n" + 
				"        \"text\": \"Joan Paulo Alves Costa\"\n" + 
				"      },\n" + 
				"      \"telecom\": [\n" + 
				"        {\n" + 
				"          \"value\": \"jpac@mailfast.tv\",\n" + 
				"          \"system\": \"email\",\n" + 
				"          \"use\": \"home\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"          \"value\": \"+5696457364\",\n" + 
				"          \"system\": \"phone\",\n" + 
				"          \"use\": \"mobile\"\n" + 
				"        }\n" + 
				"      ]\n" + 
				"    }\n" + 
				"  ],\n" + 
				"  \"telecom\": [\n" + 
				"    {\n" + 
				"      \"value\": \"lsilvasousa@micorreo.com\",\n" + 
				"      \"system\": \"email\",\n" + 
				"      \"use\": \"home\"\n" + 
				"    }\n" + 
				"  ],\n" + 
				"  \"active\": true\n" + 
				"}";
		
		render(patientJson);
	}
	
}