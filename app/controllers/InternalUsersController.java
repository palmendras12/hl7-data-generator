package controllers;

import models.Usuario;
import play.mvc.Before;

public class InternalUsersController extends UsuariosLogeadosController {

	@Before(priority = 2)
	static void validarSonCentinela() {

		Usuario u = currentUser();
		if (!u.esCentinela()) {
			unauthorized("No tiene permiso para entrar a esta sección");
		}

	}

}
