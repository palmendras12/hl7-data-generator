package controllers;

import models.*;
import play.db.jpa.GenericModel;

public class Security extends Secure.Security {

	static boolean authentify(String email, String password) {
		return Usuario.connect(email, password) != null;
	}

	static boolean check(String profile) {
		if ("admin".equals(profile)) {
			return GenericModel.find("byEmail", connected()).<Usuario>first().isAdmin;
		}

		return false;
	}

	static void onAuthenticated() {
		UsuariosLogeadosController.home();
	}

}

