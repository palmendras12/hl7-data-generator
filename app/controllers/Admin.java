package controllers;

import play.mvc.With;

@With(Secure.class)
public class Admin extends InternalUsersController {

	public static void index() {
		render();
	}

	public static void form(Long id) {
		render();
	}

}