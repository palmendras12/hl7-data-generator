package controllers;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;

import models.Paciente;
import models.Destinos.Destino;
import models.Destinos.DestinoLaboratorio;
import models.Destinos.Examen;
import models.Eventos.Evento;
import models.Eventos.MensajeADT01;
import models.Eventos.MensajeORM01;
import play.Logger;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.db.jpa.GenericModel.JPAQuery;
import play.db.jpa.JPABase;
import play.mvc.Before;
import play.mvc.Controller;
import tcpserver.MyClientSocket;

public class Application extends Controller {

	@Before
	static void addDefaults() {
		List<Destino> destinos = Destino.findAll();
		for (int i = 0; i < destinos.size(); i++) {
			Destino destino = destinos.get(i);
			destino.socketIsDisponible();
			destino.save();
		}

		renderArgs.put("destinos", destinos);

	}

	public static void index() {

		render();
	}

	public static void configurar() {

		render();
	}

	public static void testTCP(@Required Long id) {
		Destino destino = Destino.findById(id);
		destino.socketIsDisponible();

		configurar();

	}

	public static void actualizarDestino(@Valid Destino destino) {
		if (validation.hasErrors()) {
			render("@configurar", destino);
		}

		destino.save();
		configurar();

	}

	public void visualiza(@Valid Destino destino) {

		if (destino.claseDestino().equals("DestinoAdmision"))
			renderDestinoAdmision(destino);
		else if (destino.claseDestino().equals("DestinoAtencionMedico"))
			renderAtencionMedico(destino);
		else if (destino.claseDestino().equals("DestinoLaboratorio"))
			renderLaboratorio(destino);

	}

	private void renderLaboratorio(Destino destino) {

		List<MensajeORM01> examenesPendientes = buscaExamenesPendientes(destino);
		render("@renderLaboratorio", destino, examenesPendientes);
	}

	private void renderAtencionMedico(Destino destino) {
		List<MensajeADT01> admisionesPendientes = buscaAdmisionesPendientes(destino);
		render("@renderAtencionMedico", destino, admisionesPendientes);
	}

	public void renderDestinoAdmision(Destino destino) {
		render("@renderDestinoAdmision", destino);
		// render(destino, admisionesPendientes);
	}

	public static void paciente() {
		render();
	}

	public static void handlePaciente(@Valid Paciente paciente, Destino destino) throws ConstraintViolationException {

		if (validation.hasErrors()) {
			flash.error("Error de validacion. %s", paciente.nombre);
			flash.keep();
			render("@renderDestinoAdmision", destino, paciente);

			// render("@visualiza", paciente, destino);
		}

		if (Paciente.findByRut(paciente.rut) != null) {
			paciente = Paciente.findByRut(paciente.rut);
			// flash.success("Paciente existente");

		} else {
			paciente.save();
			paciente.save();
			// flash.success("Paciente nuevo creado");

		}

		// Paciente es creado si no existe y se envia el mensaje al destino.
		Logger.debug("Paciente es: %s", paciente.nombre);
		String adt01 = String.format("MSH|^~\\&|" + destino.nombre
				+ "|MC|CAMBIAME|MC|201808122322||ADT^A01|CAMBIAME|T|2.3|\n" + "EVN|A01|201808122322||\n"
				+ "PID|||MRN12345^168636490^M11|CAMBIAME|" + paciente.nombre.replace(" ", "^").toUpperCase()
				+ "|PADILLA|" + paciente.rut
				+ "|M|||AVENIDA^SANTA ROSA^326^SANTIAGO|56||(2)226120083||S||MRN12345001^82439846^M10|SS12345|15260127^STGO|\n"
				+ "NK1|1|PALMA^LESLIE^VALENZUELA|ESPOSA|||||||1|I|19910121||||AVENIDA^SANTA ROSA^326^SANTIAGO|||CHILENA||||ADM|A0|");

		// En "Cosulta Medica" Hay que poner el nombre del
		if (destino.socketIsDisponible()) {
			destino.enviarMensaje(adt01);
			paciente.save();
			flash.success("Información enviada a software: %s (%s)", destino.nombre, destino.direccion);
			// render("@mensajeEnviado", paciente, destino);

			render("@mensajeEnviado", destino);

		} else {
			flash.error("El software: %s (%s) no está conectado", destino.nombre, destino.direccion);
			flash.keep();
			index();

		}

	}

	public static void renderFichaClinica(Long idAdmision, Long destinoHis) {

		Destino destino = Destino.findById(destinoHis);
		MensajeADT01 admision = MensajeADT01.findById(idAdmision);
		List<Examen> examenes = buscaExamenPendientePaciente(admision.idAlternativo);
		// Buscando Laboratorios para solicitar examenes
		List<DestinoLaboratorio> labs = DestinoLaboratorio.findAll();
		render(admision, examenes, labs, destino);
	}

	public static void solicitaExamen(String idAlternativo, Destino destino, String examenSolicitado) {
		Paciente paciente = Paciente.findByIdAlternativo(idAlternativo);
		Logger.debug("Paciente %s  -  Solicita Examen %s a %s", idAlternativo, examenSolicitado, destino.nombre);
		if (paciente == null) {
			Logger.debug("En el examen solicitado el paciente no existe");
			error("En el examen solicitado el paciente no existe");
		}

		// Enviamos mensaje HL7 a destino seleccionado

		if (destino.socketIsDisponible()) {

			// Examen examen = new Examen(examenSolicitado, idAlternativo);
			// examen.save();
			// paciente.agregarExamen(examen);
			paciente.save();

			String orm = String.format("MSH|^~\\&|" + destino.nombre
					+ "|MC|CAMBIAME|MC|201808122322||ORM^O01|CAMBIAME|P|2.3|\n" + "EVN|A01|201808122322||\n"
					+ "PID|||MRN12345^168636490^M11|" + paciente.idAlternativo + "|"
					+ paciente.nombre.replace(" ", "^").toUpperCase() + "|PADILLA|" + paciente.rut
					+ "|M||M|AVENIDA^SANTA ROSA^326^SANTIAGO|56||(2)226120083||S||MRN12345001^82439846^M10|SS12345|15260127^STGO|\n"
					+ "PV1||A|CAE^CARDIOLOGIA||||2323^ALMENDRAS^PABLO|||CAE|||||||||1|||||||||||||||||||||||||20180812114500|\n"
					// Ver si este es el ID de la orden.
					+ "ORC|NV|2018081211450000000\n" + "OBR|1|2018081211450000000||" + "noCode" + "^" + examenSolicitado
					+ "|||20180812114500");

			destino.enviarMensaje(orm);
			flash.success("Información enviada a software: %s (%s)", destino.nombre, destino.direccion);
			// render("@mensajeEnviado", paciente, destino);

			render("@mensajeExamenEnviado", destino, paciente);

		} else {
			flash.error("El software: %s (%s) no está conectado", destino.nombre, destino.direccion);
			flash.keep();
			index();

		}
	}

	public static void procesarAtencion() {

	}

	private static List<MensajeADT01> buscaAdmisionesPendientes(Destino destino) {
		List<MensajeADT01> admisionesPendientes = new ArrayList<>();
		List<MensajeADT01> mensajesAdmision = MensajeADT01.findAll();
		for (int i = 0; i < mensajesAdmision.size(); i++) {
			MensajeADT01 mensaje = mensajesAdmision.get(i);

			Logger.debug("Mensaje procesado: %s   %s -- %s", mensaje.procesado, mensaje.aplicacionRecepcion,
					destino.nombre);

			if (mensaje.procesado == false && mensaje.aplicacionRecepcion.equals(destino.nombre)) {
				admisionesPendientes.add(mensaje);
			}
		}
		return admisionesPendientes;
	}

	private static List<MensajeORM01> buscaExamenesPendientes(Destino destino) {
		List<MensajeORM01> admisionesPendientes = new ArrayList<>();
		List<MensajeORM01> mensajesExamen = MensajeORM01.findAll();
		for (int i = 0; i < mensajesExamen.size(); i++) {
			MensajeORM01 mensaje = mensajesExamen.get(i);

			Logger.debug("Mensaje procesado: %s   %s -- %s", mensaje.realizado, mensaje.aplicacionRecepcion,
					destino.nombre);

			if (mensaje.realizado == false && mensaje.aplicacionRecepcion.equals(destino.nombre)) {
				admisionesPendientes.add(mensaje);
			}
		}
		return admisionesPendientes;
	}

	private static List<Examen> buscaExamenPendientePaciente(String idAlternativo) {
		Logger.debug("Buscando paciente cno id Alternativo %s", idAlternativo);
		Paciente paciente = Paciente.findByIdAlternativo(idAlternativo);
		return paciente.examenes;
	}

	public static void informarExamen(MensajeORM01 solicitudExamen, Destino destino, String resultadoExamen) {

		Logger.debug("Informando ex. %s id %s - resultado %s", solicitudExamen.textoSolicitudORU,
				solicitudExamen.idMensaje, resultadoExamen);
		Logger.debug("Enviando a %s (%s)", destino.nombre, destino.direccion);

		if (destino.socketIsDisponible()) {
			String oru = String.format("MSH|^~\\&|" + destino.nombre
					+ "|MC|CAMBIAME|MC|201808122322||ORU^R01|CAMBIAME|P|2.3|\n" + "PID|||MRN12345^168636490^M11|"
					+ solicitudExamen.idAlternativo + "|" + solicitudExamen.primerNombrePaciente + "^"
					+ solicitudExamen.apellidoNombrePaciente + "|PADILLA|" + solicitudExamen.rutPaciente
					+ "|M||M|AVENIDA^SANTA ROSA^326^SANTIAGO|56||(2)226120083||S||MRN12345001^82439846^M10|SS12345|15260127^STGO|\n"
					+ "ORC|NV|2018081211450000001|INF2018081211450000001||||||20180812144500|||HIS\n"
					+ "OBR|1|2018081211450000001|INF2018081211450000001|27171005^ANALISIS DE ORINA\n"
					+ "OBX|1|ST|27171005^ANALISIS DE ORINA|PROTEINA|" + resultadoExamen + "|MG");

			destino.enviarMensaje(oru);
			flash.success("Información enviada a software: %s (%s)", destino.nombre, destino.direccion);
			// render("@mensajeEnviado", paciente, destino);

			render("@mensajeResultadoExamenEnviado", destino);
		} else {
			flash.error("El software: %s (%s) no está conectado", destino.nombre, destino.direccion);
			flash.keep();
			index();

		}
	}

	public static void cierraAtencion(String idAlternativo) {

		Paciente paciente = Paciente.findByIdAlternativo(idAlternativo);

		MensajeADT01 admision = MensajeADT01.find("byIdAlternativo", idAlternativo).first();
		admision.procesado = true;
		admision.save();
		flash.success("Paciente %s  (%s) marcado como descarga", paciente.nombre, paciente.idAlternativo);
		flash.keep();
		index();

	}

	public void restPaciente(String idAlternativo) {

		Paciente paciente = Paciente.findByIdAlternativo(idAlternativo);
		String entrega = paciente.entregaInforme();
		
		// ENVIA 
		
		try {
			InetAddress direccion = InetAddress.getByName("152.74.29.21");
			MyClientSocket socket = new MyClientSocket(direccion, 6663);
			if (socket.isConnected()) {
				socket.sendMsgTCP(entrega);
			} else {

				Logger.error("Socket no conectado");

			}

		} catch (Exception ex) {
			Logger.error("Error al conectar a a IP %s", ex);
		}
		
		renderJSON(entrega);
	}

	public static void monitor() {
	
		List <Evento> eventos = Evento.findAll();
		render(eventos);
	}

}