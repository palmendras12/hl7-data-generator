package controllers;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import models.Destinos.Destino;
import models.Destinos.DestinoConcreto;
import models.Destinos.TipoDestino;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.mvc.Before;
import play.mvc.With;

@With(Secure.class)
public class PanelController extends InternalUsersController {
	
	@Before
	public static void before() {
		List <Destino> destinos= Destino.findAll();
		List<TipoDestino> tiposDestinos = TipoDestino.findAll();
		renderArgs.put("destinos", destinos);
		renderArgs.put("tiposDestinos", tiposDestinos);

	}
	public static void index() {

		render();
	}

	public static void formAgregarDestino() {
		Destino destino = new DestinoConcreto();	
		render(destino);
	}

	public static void agregarDestino(@Valid DestinoConcreto destino) throws IllegalAccessException, InvocationTargetException {
	
		if (validation.hasErrors()) {
			render("@formAgregarDestino", destino);
		}
		Destino destinoASalvar = destino.tipo.crearDestino();
		BeanUtils.copyProperties(destinoASalvar, destino);


		
		destinoASalvar.tipo = destino.tipo;
		destinoASalvar.creacion = LocalDateTime.now();
		destinoASalvar.save();

		flash.success("Nuevo Destino %s guardado", destinoASalvar.tipo.nombre);
		flash.keep();
		destinos();
	}

	public static void destinos() {
		
		render();
	}
	public static void destino(@Required Long id) {
	
		Destino destino = Destino.findById(id);
		render("@formAgregarDestino", destino);
	}
	
}
