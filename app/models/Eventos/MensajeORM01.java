package models.Eventos;

import java.util.List;

import javax.persistence.Entity;

import models.Paciente;
import models.Destinos.Examen;
import play.Logger;
import tcpserver.MensajeHL7;

@Entity
public class MensajeORM01 extends MensajeHL7 {

	public Boolean realizado = false;
	// MSH
	public String aplicaciondeEnvio;
	public String facilitadorEnvio;
	public String aplicacionRecepcion;
	public String facilitadorRecepción;
	public String fechaMensaje;
	public String tipoMensaje;
	public String idControldeMensaje;
	public String idProceso;
	public String idVersión;
	public String idAlternativo;
	public String idExamenSolicitud;
	public String idPacienteNacional;
	public String idPacienteDNI;
	public String idPacienteInterno;
	public String textoSolicitudORU;
	public String primerNombrePaciente;
	public String apellidoNombrePaciente;
	public String rutPaciente;
	public String codigoSolicitudExamen;

	public MensajeORM01(String linea) {
		super(linea);

		// Parsear HL7 y guardar los datos
		String[] split = linea.split("\\|");
		Logger.debug("hay %d elementos", split.length);
		for (int i = 0; i < split.length-1; i++) {
			Logger.debug("%d - %s", i, split[i]);
		}
		this.aplicaciondeEnvio = split[2];
		this.facilitadorEnvio = split[3];
		this.aplicacionRecepcion = split[4];
		this.facilitadorEnvio = split[5];
		this.fechaMensaje = split[6];
		this.tipoMensaje = split[8];

		String[] idString = split[34].split("\\^");
		this.idPacienteNacional = idString[0];

		this.idPacienteDNI = idString[1];

		this.idPacienteInterno = idString[2];


		this.idAlternativo = split[20];

		String[] infoExamen = split[88].split("\\^");


		this.idExamenSolicitud = infoExamen[0];

		this.textoSolicitudORU = infoExamen[1];
		
		String[] nombresString = split[21].split("\\^");

		this.primerNombrePaciente = nombresString[1];

		this.apellidoNombrePaciente = nombresString[0];

		this.rutPaciente = split[18];
		
		this.codigoSolicitudExamen= split[84].replaceAll("[\n\rOBR]","");
		Logger.debug("----->codigoSolicitud del ORM %s",split[84]);

		Logger.debug("----->codigoSolicitud del ORM %s", this.codigoSolicitudExamen);
		Logger.debug("----->cdigo exmen del ORM %s", this.idExamenSolicitud);


	}

	@Override
	public void realizarAccion() {

		Logger.debug("Mensade ORM01 realiza accion");
		Logger.debug("%s %s", this.primerNombrePaciente, this.apellidoNombrePaciente);
		
		// Verificar si no existe un ORMR01 de admisión pendiente con un ID indicado en
		// el ORM Interno similar.
		List<MensajeORM01> msg = MensajeORM01.find("byIdAlternativoAndrealizado", this.idAlternativo, false).fetch();
		Logger.debug("%s", this.idAlternativo);

		if (this.idAlternativo.equals("")) {

			Logger.debug("Mnesaje viene sin ID Alternativo");
			this.valido = false;
			Evento evento = new Evento(String.format("Mensaje viene sin ID Alternativo"),this.ipOrigen);
			evento.save();

		} 
		else if(Paciente.findByIdAlternativo(this.idAlternativo)==null){
			Logger.debug("Intentando agregar examen a paciente que no existe");
			Evento evento = new Evento(String.format("Intentando agregar examen a paciente que no existe (%s)",this.idAlternativo),this.ipOrigen);
			evento.save();
		}
		else if (this.idExamenSolicitud.equals("27171005")) {
			this.valido = true;
			
			Examen examen = new Examen("ANALISIS DE ORINA (PROCEDIMIENTO)",this.idExamenSolicitud,this.codigoSolicitudExamen,this.idAlternativo,this.idMensaje);
			Paciente paciente = Paciente.findByIdAlternativo(this.idAlternativo);
			paciente.agregarExamen(examen);
			paciente.save();
			Logger.debug("ANALISIS DE ORINA (PROCEDIMIENTO) %s", this.idExamenSolicitud);
			Evento evento = new Evento(String.format("Se solicitó un examen: ANALISIS DE ORINA (PROCEDIMIENTO) para paciente %s", this.idAlternativo),this.ipOrigen);
			evento.save();
		} else if (this.idExamenSolicitud.equals("306992007")) {
			this.valido = false;

		}

		else {
			Logger.debug("Codigo de mensaje no soportado %s", this.idExamenSolicitud);
			Evento evento = new Evento(String.format("El codigo %s no es reconocido por el sistema", this.idExamenSolicitud),this.ipOrigen);
			evento.save();
		}

	}

}
