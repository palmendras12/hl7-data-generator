package models.Eventos;

import java.nio.charset.Charset;
import java.util.List;

import javax.persistence.Entity;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;

import models.Paciente;
import play.Logger;
import play.db.jpa.JPABase;
import tcpserver.MensajeHL7;

@Entity
public class MensajeADT01 extends MensajeHL7 {

	public String mensaje;
	public Boolean procesado = false;

	// MSH
	public String aplicaciondeEnvio;
	public String facilitadorEnvio;
	public String aplicacionRecepcion;
	public String facilitadorRecepción;
	public String fechaMensaje;
	public String tipoMensaje;
	public String idControldeMensaje;
	public String idProceso;
	public String idVersión;

	// EVN

	public String fechaRegistro;
	public String codigoTipoEveto;
	public String idOperacion;

	public String fechaEvento;

	public String tipoEvento;

	public String idPacienteNacional;

	public String idPacienteDNI;

	public String idPacienteInterno;

	public String primerNombrePaciente;

	public String apellidoNombrePaciente;

	public String segundoApellidoNombrePaciente;

	public String fechaNacimientoPaciente;

	public String sexoPaciente;

	public String razaPaciente;

	public String direccionTipoVia;

	public String direccionNombreVia;

	public String direccionNumero;

	public String direccionCiudad;

	public String codigoPais;

	public String telefonoPaciente;

	public String estadoMarital;
	public String idAlternativo;

	// PID
	// id Paciente.NHC Nacional
	// id Paciente.DNI
	// id Paciente.NHC Interno
	// nombre Paciente.Apellido Paterno
	// nombre Paciente.Nombre
	// nombre Paciente.Apellido Materno
	// fecha Nacimiento
	// sexo
	// raza
	// dirección Paciente.Tipo Vía
	// dirección Paciente.Nombre Vía
	// dirección Paciente.Número
	// dirección Paciente.Ciudad
	// codigoPais
	// numeroTelefonoTrabajo
	// estadoMarital
	// numeroCuentaPaciente
	// Número Cuenta Paciente.
	// Número Cuenta Paciente.
	// Número Paciente SS
	// Número Licencia Conducir.Número
	// Número Licencia Conducir.Ciudad
	// Grupo Étinco

	// NK1
	// ID Pariente Cercano
	// Nombre Pariente Cercano.Apellido Paterno
	// Nombre Pariente Cercano.Nombre
	// Nombre Pariente Cercano.Apellido Materno
	// Relación
	// Parte Asociada al Título de Trabajo
	// Código Asociado a las Partes
	// Fecha Nacimiento
	// Dirección Paciente.Tipo Vía
	// Dirección Paciente.Nombre Vía
	// Dirección Paciente.Número
	// Dirección Paciente.Ciudad
	// Ciudadanía
	// Indicador Protección
	// Indicador Estudio

	public MensajeADT01(String line) {
		super(line);
		// Parsear HL7 y guardar los datos
		String[] split = line.split("\\|");
		this.aplicaciondeEnvio = split[2];
		this.facilitadorEnvio = split[3];
		this.aplicacionRecepcion = split[4];
		this.facilitadorEnvio = split[5];
		this.fechaMensaje = split[6];
		this.tipoMensaje = split[8];

		this.tipoEvento = split[13];
		this.fechaEvento = split[14];

		String[] idString = split[19].split("\\^");

		this.idPacienteNacional = idString[0];

		this.idPacienteDNI = idString[1];

		this.idPacienteInterno = idString[2];

		this.idAlternativo = split[20];

		String[] nombresString = split[21].split("\\^");

		this.primerNombrePaciente = nombresString[1];

		this.apellidoNombrePaciente = nombresString[0];

		this.segundoApellidoNombrePaciente = split[22];

		this.fechaNacimientoPaciente = split[23];

		this.sexoPaciente = split[24];

		this.razaPaciente = split[26];

		String[] direccionString = split[27].split("\\^");

		this.direccionTipoVia = direccionString[0];
		this.direccionNombreVia = direccionString[1];
		this.direccionNumero = direccionString[2];
		this.direccionCiudad = direccionString[3];
		this.codigoPais = split[28];
		this.telefonoPaciente = split[30];
		this.estadoMarital = split[32];
	}

	@Override
	public void realizarAccion() {

		Logger.debug("Mensade ADT01 realiza accion");
		// Verificar si no existe un mensaje con un ID Interno similar.
		List<MensajeADT01> msg = MensajeADT01.find("byIdAlternativoAndProcesado", this.idAlternativo, false).fetch();
		if (msg.size() >= 1) {
			Logger.debug("Hay %d mensajes con el id %s", msg.size(), this.idAlternativo);
			Evento evento = new Evento(String.format("El paciente %s ya está admitido", this.idAlternativo),
					this.ipOrigen);
			evento.save();

			this.valido = false;

		} else
			this.valido = true;

	}

}
