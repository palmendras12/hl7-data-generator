package models.Eventos;

import java.util.List;

import javax.persistence.Entity;

import models.Paciente;
import models.Destinos.Examen;
import play.Logger;
import tcpserver.MensajeHL7;

@Entity
public class MensajeORU01 extends MensajeHL7 {

	public String aplicaciondeEnvio;
	public String facilitadorEnvio;
	public String aplicacionRecepcion;
	public String facilitadorRecepción;
	public String fechaMensaje;
	public String tipoMensaje;
	public String idControldeMensaje;
	public String idProceso;
	public String idVersión;
	public String idAlternativo;
	public String idSolicitudORU;
	public String idPacienteNacional;
	public String idPacienteDNI;
	public String idPacienteInterno;
	public String textoSolicitudORU;
	public String codigoSolicitudExamen;
	public String rutPaciente;
	public String primerNombrePaciente;
	public String apellidoNombrePaciente;
	public String examenText;
	public String examenValueResult;
	public String examenUnitResult;
	
	public MensajeORU01(String linea) {
		super(linea);

		// Parsear HL7 y guardar los datos
				String[] split = linea.split("\\|");
				for (int i = 0; i < split.length; i++) {
					Logger.debug("%d - %s", i, split[i]);
				}
				
				this.aplicaciondeEnvio = split[2];
				this.facilitadorEnvio = split[3];
				this.aplicacionRecepcion = split[4];
				this.facilitadorEnvio = split[5];
				this.fechaMensaje = split[6];
				this.tipoMensaje = split[8];

				String[] idString = split[15].split("\\^");

				this.idPacienteNacional = idString[0];

				this.idPacienteDNI = idString[1];
				this.rutPaciente = idString[1];

				this.idPacienteInterno = idString[2];
				

				this.idAlternativo = split[16];
				
				String[] nombresString = split[17].split("\\^");

				this.primerNombrePaciente = nombresString[1];

				this.apellidoNombrePaciente = nombresString[0];

				
				
				this.codigoSolicitudExamen= split[35];
				
				String[] exmenString = split[52].split("\\^");

				this.examenText= exmenString[1];
				this.examenValueResult =split[54];
				this.examenUnitResult =split[55];
				
	}

	@Override
	public void realizarAccion() {

		Logger.debug("Mensade ORU01 realiza accion");
		
		//Primero busco si existe la solicitud ingresada
		Logger.debug("Buscando examen conn %s", this.codigoSolicitudExamen);
		List<Examen> msg = Examen.find("byIdSolicitudOruAndRealizado", this.codigoSolicitudExamen,false).fetch();
		Logger.debug("Array examenes %d", msg.size());
		if (msg.size()<1) {
			Logger.debug("Llego ORU01 pero no existe una orden pendiente con codigo %s", this.codigoSolicitudExamen);
			Evento evento = new Evento(String.format("Codigo del informe de examen no existe en el sistema (%s)",this.codigoSolicitudExamen),this.ipOrigen);
			evento.save();
			
		}
		else if(msg.size()>1){
			Logger.debug("Existen multiples ordenes con codigo %s (se informaran todas con el mismo resultado)", this.codigoSolicitudExamen);
			
			Evento evento = new Evento(String.format("Multiples registros de orden (%s). Se actualizaron todas con este informe.",this.codigoSolicitudExamen),this.ipOrigen);
			evento.save();
			
			for (int i = 0; i < msg.size(); i++) {
				String resultado=String.format("%s %s", this.examenValueResult, this.examenUnitResult);
				msg.get(i).realizado=true;
				msg.get(i).resultado=resultado;
				msg.get(i).save();

			}
			String resultado=String.format("%s %s", this.examenValueResult, this.examenUnitResult);
			Examen.findByidSolicitudORU(this.codigoSolicitudExamen, resultado);


		}
		else {
			
			Logger.debug("Existe orden con codigo %s (se informa)", this.codigoSolicitudExamen);
			
			String resultado=String.format("%s %s", this.examenValueResult, this.examenUnitResult);
			msg.get(0).realizado=true;
			msg.get(0).resultado=resultado;
			msg.get(0).save();

			//Examen examen = Examen.find("byIdSolicitudOru", this.codigoSolicitudExamen).first();
			//String resultado=String.format("%s %s", this.examenValueResult, this.examenUnitResult);
			//Examen.findByidSolicitudORU(this.codigoSolicitudExamen, resultado);

			this.valido=true;
		}
	}

}
