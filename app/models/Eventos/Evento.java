package models.Eventos;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import models.Paciente;
import models.Destinos.Destino;
import play.db.jpa.Model;

@Entity
public class Evento extends Model {

	public LocalDateTime creacion;

	public String mensaje;

	public String ipOringen;

	public Evento(String mensaje, String ipOrigen) {
		this.creacion = LocalDateTime.now();
		this.mensaje = mensaje;
		this.ipOringen = ipOrigen;
	}

}
