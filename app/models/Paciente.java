package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import models.Destinos.Examen;
import models.Eventos.Evento;
import play.Logger;
import play.data.validation.MinSize;
import play.data.validation.Required;
import play.db.jpa.Model;

@Entity
public class Paciente  extends Model{

	@MinSize(3)
	@Required
	public String nombre;

	@MinSize(3)
	@Required
	public String rut;


	@OneToMany(mappedBy = "paciente", cascade = CascadeType.ALL)
	public List<Examen> examenes = new ArrayList<>();
	
	//@Required
	//@Column(unique = true, nullable = false)
	public String idAlternativo;

	public Paciente(String nombre, String rut, String idAlternativo) {
		this.nombre = nombre;
		this.rut = rut;
		this.idAlternativo = idAlternativo;
	}


	public static Paciente findByRut(String rut2) {
		return Paciente.find("byRut", rut2).first();
	}
	
	public static Paciente findByIdAlternativo(String idAlternativo) {
		Logger.debug("Buscando paciente %s", idAlternativo);
		return Paciente.find("byIdAlternativo", idAlternativo).first();
	}


	public void agregarExamen(Examen examen) {

		this.examenes.add(examen);
		examen.paciente = this;
		examen.save();
	}
	
	public String entregaInforme() {
		
		String informe="{\n" + 
				"  \"Paciente\": {\n" + 
				"    \"nombre\": \""+this.nombre+"\",\n" + 
				"    \"id\": \""+this.idAlternativo+"\"  },\n" + 
				"    \"Examenes\": [\n";

		for (int i = 0; i < this.examenes.size(); i++) {
			informe+=String.format("%s",this.examenes.get(i).resumenTexto()+", \n");
		}
		informe+=				"      ]\n" + 
				"}";
		Logger.debug("%s", informe);
		return informe;
		
	}

}
