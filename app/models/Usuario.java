package models;

import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import play.data.validation.Email;
import play.data.validation.Required;
import play.db.jpa.Model;

@Entity
public class Usuario extends Model {

	@Email
	@Required
	public String email;

	@Required
	public String password;
	public String fullname;
	public boolean isAdmin;


	
	public Usuario(String email, String password, String fullname) {
		this.email = email;
		this.password = password;
		this.fullname = fullname;
	}

	public Usuario() {
		// TODO Auto-generated constructor stub
	}

	public static Usuario connect(String email, String password) {
		return find("byEmailAndPassword", email.trim().toLowerCase(), password).first();
	}

	@Override
	public String toString() {
		return fullname;
	}

	public String getEmail() {
		return email;
	}

	public boolean esCentinela() {
		return this.email.equals("pablo@centinela.cl");
	}

	@PrePersist
	@PreUpdate
	public void normalizarDatos() {

		this.email = this.email.toLowerCase().trim();

	}

	public String getNombre() {
		return fullname;
	}

	

}