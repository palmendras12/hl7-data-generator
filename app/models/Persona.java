package models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.OneToOne;

import play.data.validation.MinSize;
import play.data.validation.Required;
import play.db.jpa.Model;

@Entity
@Inheritance
public class Persona extends Model{

	@MinSize(3)
	@Required
	@Column(unique = true, nullable = false)
	public String nombre;
	
	@MinSize(3)
	@Required
	@Column(unique = true, nullable = false)
	public String rut;
		
	public Persona(String nombre, String rut) {
		this.nombre = nombre;
		this.rut = rut;
	}

	
	
}
