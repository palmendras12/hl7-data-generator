package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Medico extends Persona {

	public Especialidad especialidad;
	@OneToMany
	public List<Paciente> pacientes = new ArrayList<>();

	public Medico(String nombre, String rut, String email, Especialidad especialidad) {
		super(nombre, rut);
		this.especialidad = especialidad;
	}

}
