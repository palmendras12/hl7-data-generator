package models.Destinos;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import models.Paciente;
import play.Logger;
import play.db.jpa.Model;

@Entity
public class Examen extends Model {

	public String idSolicitudORU="NoCode";
	public String idAlternativo;
	public Boolean realizado = false;

	@ManyToOne
	public Paciente paciente;
	public String idExamen;
	public String resultado="";
	public String codigoExamen;
	public String nombreExamen;

	
	public Examen(String nombreExamen, String codigoExamen,String idSolicitudORU, String idAlternativo, String idMensaje) {

		this.nombreExamen = nombreExamen;
		this.codigoExamen = codigoExamen;
		this.idAlternativo = idAlternativo;
		this.idExamen = idMensaje;
		this.idSolicitudORU=idSolicitudORU;
	}
	public Examen(String nombreExamen, String idAlternativo, String idAlternativo2) {

		this.nombreExamen = nombreExamen;
		this.idAlternativo = idAlternativo;
	}
	
	public static void findByidSolicitudORU(String solicitudORU, String resultado) {
		Logger.debug("Busca examen");
		Examen d = Examen.find("lower(trim(idSolicitudORU)) = ?1", solicitudORU).first();
		Logger.debug("Examen %s encontrado", solicitudORU);
		d.resultado=resultado;
		d.realizado=true;
		d.save();
	}
	
	public String resumenTexto() {
		return String.format("%s (%s)   RESULTADO: %s    id Solicitud [%s]", this.nombreExamen,this.codigoExamen,this.resultado, this.idSolicitudORU);
	}
	

}
