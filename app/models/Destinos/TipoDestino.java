package models.Destinos;

import javax.persistence.Entity;
import javax.persistence.Inheritance;

import org.apache.commons.lang.math.RandomUtils;

import play.db.jpa.Model;

@Entity
@Inheritance
public abstract class TipoDestino extends Model {

	public String nombre;

	public final Destino crearDestino() {

		Destino dest = dameInstanciaDestino();

		return dest;
	}

	protected abstract Destino dameInstanciaDestino();

}
