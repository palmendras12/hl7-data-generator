package models.Destinos;

import javax.persistence.Entity;

@Entity
public class DestinoConcreto extends Destino {

	public DestinoConcreto() {
		super();
	}

	public DestinoConcreto(String nombreDestino, String ipDestino, int port) {
		super(nombreDestino, ipDestino, port);
	}

	@Override
	public void enviarMensaje(String mensaje) {
		
	}



}
