package models.Destinos;

import javax.persistence.Entity;

@Entity
public class TipoDestinoLaboratorio extends TipoDestino {

	@Override
	protected Destino dameInstanciaDestino() {
		return new DestinoLaboratorio();
	}

}
