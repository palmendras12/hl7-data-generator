package models.Destinos;

import java.net.InetAddress;

import javax.persistence.Entity;

import play.Logger;
import tcpserver.MensajeHL7;
import tcpserver.MyClientSocket;

@Entity
public class DestinoAdmision extends Destino {

	public DestinoAdmision() {
		super();
	}

	public DestinoAdmision(String nombreDestino, String ipDestino, int port) {
		super(nombreDestino, ipDestino, port);
	}

	// El mensaje por defecto de para este Destino de Admisión es un PID
	@Override
	public void enviarMensaje(String mensaje) {

		try {
			InetAddress direccion = InetAddress.getByName(this.direccion);
			MyClientSocket socket = new MyClientSocket(direccion, this.port);
			if (socket.isConnected()) {
				socket.sendMsgTCP(mensaje);
			} else {

				Logger.error("Socket no conectado");

			}

		} catch (Exception ex) {
			Logger.error("Error al conectar a a IP %s", ex);
		}

	//El mensaje por defecto de para este Destino de Admisión es un PID
	
	}

}
