package models.Destinos;

import java.net.InetAddress;

import javax.persistence.Entity;

import play.Logger;
import tcpserver.MyClientSocket;

@Entity
public class DestinoLaboratorio extends Destino {

	@Override
	public void enviarMensaje(String mensaje) {
		
		try {
			InetAddress direccion = InetAddress.getByName(this.direccion);
			MyClientSocket socket = new MyClientSocket(direccion, this.port);
			if (socket.isConnected()) {
				socket.sendMsgTCP(mensaje);
			} else {

				Logger.error("Socket no conectado");

			}

		} catch (Exception ex) {
			Logger.error("Error al conectar a a IP %s", ex);
		}
		

	}

	public DestinoLaboratorio() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DestinoLaboratorio(String nombreDestino, String ipDestino, int port) {
		super(nombreDestino, ipDestino, port);
		// TODO Auto-generated constructor stub
	}

}
