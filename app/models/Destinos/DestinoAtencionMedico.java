package models.Destinos;

import java.net.InetAddress;

import javax.persistence.Entity;

import play.Logger;
import tcpserver.MyClientSocket;

@Entity
public class DestinoAtencionMedico extends Destino {

	public DestinoAtencionMedico() {
		super();
	}

	public DestinoAtencionMedico(String nombreDestino, String ipDestino, int port) {
		super(nombreDestino, ipDestino, port);
	}
	


	@Override
	public void enviarMensaje(String mensaje) {
		// TODO Auto-generated method stub
		try {
			InetAddress direccion = InetAddress.getByName(this.direccion);
			MyClientSocket socket = new MyClientSocket(direccion, this.port);
			if (socket.isConnected()) {
				socket.sendMsgTCP(mensaje);
			} else {

				Logger.error("Socket no conectado");

			}

		} catch (Exception ex) {
			Logger.error("Error al conectar a a IP %s", ex);
		}
	}

}
