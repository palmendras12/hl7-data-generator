package models.Destinos;

import javax.persistence.Entity;

@Entity
public class TipoDestinoAtencionMedico extends TipoDestino {

	@Override
	protected Destino dameInstanciaDestino() {
		
		return new DestinoAtencionMedico();
	}

}
