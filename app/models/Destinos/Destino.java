package models.Destinos;

import java.net.InetAddress;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import models.Eventos.Evento;
import play.data.validation.Min;
import play.data.validation.MinSize;
import play.data.validation.Required;
import play.db.jpa.Model;
import tcpserver.MensajeHL7;
import tcpserver.MyClientSocket;

@Inheritance
@Entity
public abstract class Destino extends Model {

	@MinSize(3)
	@Required
	@Column(unique = true, nullable = false)
	public String nombre;
	@MinSize(3)
	@Required
	@Column(unique = true, nullable = false)
	public String direccion;
	@Min(1000)
	@Required
	public Integer port;

	private boolean isConnected;

	@ManyToOne(optional = false)
	@Required
	public TipoDestino tipo;

	public LocalDateTime creacion;

	@OneToMany
	public List<Enlace> enlaces = new ArrayList<>();

	@OneToMany
	public List<Evento> eventos = new ArrayList<>();
	
	public Destino(String nombreDestino, String ipDestino, int port) {
		this.nombre = nombreDestino;
		this.direccion = ipDestino;
		this.port = port;

	}

	public Destino() {
	}

	public boolean socketIsDisponible() {
		try {
			InetAddress direccion = InetAddress.getByName(this.direccion);
			MyClientSocket socket = new MyClientSocket(direccion, this.port);
			if (socket.isConnected()) {
				this.isConnected = true;
				return true;
			} else {

				this.isConnected = false;
				return false;

			}

		} catch (Exception ex) {
			this.isConnected = false;
			return false;

		}
	}

	public String claseDestino() {
		Class<? extends Destino> child = this.getClass(); // child class object
		String simpleName = child.getSimpleName();
		return simpleName;
	}

	public abstract void enviarMensaje(String mensaje);
	
	//public abstract MensajeHL7 creaMensajeHL7();

}
