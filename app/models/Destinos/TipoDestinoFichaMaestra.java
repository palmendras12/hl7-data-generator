package models.Destinos;

import javax.persistence.Entity;

@Entity
public class TipoDestinoFichaMaestra extends TipoDestino {

	@Override
	protected Destino dameInstanciaDestino() {
		return new DestinoFichaMaestra();
	}

}
